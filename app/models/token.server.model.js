'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    crypto = require('crypto');

/**
 * Tokens Schema
 */
var TokenSchema = new Schema({
    token: {
        type: String,
        trim: true
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    created: {
        type: Date,
        default: Date.now
    }
});

/**
 * Find possible not used username
 */
TokenSchema.statics.findUnique = function(tokenIn, callback) {
    var _this = this;

    _this.findOne({
        token: tokenIn
    }, function(err, token) {
        if (!err) {
            if (!token) {
                callback(null);
            } else {
                return _this.findUnique(token, callback);
            }
        } else {
            callback(null);
        }
    });
};

mongoose.model('Token', TokenSchema);