'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	crypto = require('crypto');

/**
 * A Validation function for local strategy properties
 */
var validateLocalStrategyProperty = function(property) {
	return ((this.provider !== 'local' && !this.updated) || property.length);
};

/**
 * A Validation function for local strategy password
 */
var validateLocalStrategyPassword = function(password) {
	return (this.provider !== 'local' || (password && password.length > 6));
};

/**
 * User Schema
 */
var UserSchema = new Schema({
    pais: {
        type: String,
        trim: true,
        default: ''

    },telefono_empresa: {
        type: Number,
        trim: true,
        default: ''
    },afinidad: {
        type: String,
        trim: true,
        default: ''
    },telefono_habitacion: {
        type: Number,
        trim: true,
        default: ''
    },telefono_movil: {
        type: Number,
        trim: true,
        default: ''
    },nacionalidad: {
        type: String,
        trim: true,
        default: ''
    },sexo: {
        type: String,
        trim: true,
        default: ''
    },profesion: {
        type: String,
        trim: true,
        default: ''
    },estado_civil: {
        type: String,
        trim: true,
        default: ''
    },empresa: {
        type: String,
        trim: true,
        default: ''
    },tipo_via: {
        type: String,
        trim: true,
        default: ''
    },urbanizacion: {
        type: String,
        trim: true,
        default: ''
    },poblacion: {
        type: String,
        trim: true,
        default: ''
    },
    direccion: {
        type: String,
        trim: true,
        default: ''
    },
    parroquia: {
        type: String,
        trim: true,
        default: ''
    },
    municipio: {
        type: String,
        trim: true,
        default: ''
    },
    idEscolar: {
        type: Number

    },
    estado: {
        type: String,
        trim: true,
        default: ''
    },
    fecha_nacimiento: {
        type: Date,
        trim: true,
        default: ''
    },
    cedula: {
        type: Number,
        trim: true,
        default: '',
        validate: [validateLocalStrategyProperty, 'Por favor ingrese su número de cédula']
    },
    nombres: {
		type: String,
		trim: true,
		default: '',
		validate: [validateLocalStrategyProperty, 'Por favor ingrese su nombre']
	},
	apellidos: {
		type: String,
		trim: true,
		default: '',
		validate: [validateLocalStrategyProperty, 'Por favor ingrese su apellido']
	},
	displayName: {
		type: String,
		trim: true
	},
	email: {
		type: String,
		trim: true,
		default: '',
		validate: [validateLocalStrategyProperty, 'Por favor ingrese su correo'],
		match: [/.+\@.+\..+/, 'por favor ingrese una cuenta de correo valida']
	},
	username: {
		type: String,
		unique: 'testing error message',
		required: 'Por favor ingrese su usuario',
		trim: true
	},
	password: {
		type: String,
		default: '',
		validate: [validateLocalStrategyPassword, 'La contraseña debe contener al menos 6 caracteres']
	},
	salt: {
		type: String
	},
	provider: {
		type: String,
		required: 'Provider is required'
	},
	providerData: {},
	additionalProvidersData: {},
	roles: {
		type: [{
			type: String,
			enum: ['user', 'admin']
		}],
		default: ['user']
	},
	updated: {
		type: Date
	},
	created: {
		type: Date,
		default: Date.now
	},
	/* For reset password */
	resetPasswordToken: {
		type: String
	},
  	resetPasswordExpires: {
  		type: Date
  	},location: {
        type: String
    },
    website: {
        type: String
    },
    bio: {
        type: String
    },
    avatar: {
        type: String
    },
    profileBg: {
        type: String
    }
});

/**
 * Hook a pre save method to hash the password
 */
UserSchema.pre('save', function(next) {
	if (this.password && this.password.length > 6) {
		this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
		this.password = this.hashPassword(this.password);
	}

	next();
});

/**
 * Create instance method for hashing a password
 */
UserSchema.methods.hashPassword = function(password) {
	if (this.salt && password) {
		return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
	} else {
		return password;
	}
};

/**
 * Create instance method for authenticating user
 */
UserSchema.methods.authenticate = function(password) {
	return this.password === this.hashPassword(password);
};

/**
 * Find by Pk
 */
UserSchema.statics.findById = function(id, callback) {
	var _this = this;

	_this.findOne({
		_id: id
	}, function(err, user) {
		if (!err) {
			if (!user) {
				callback(id);
			} else {
				return _this.findById(id, callback);
			}
		} else {
			callback(null);
		}
	});
};


/**
 * Find all users
 */
UserSchema.statics.findAll = function(callback) {
	var _this = this;

	_this.find({}, function(err, users) {
		if (!err) {
			if (!users) {
				callback(users);
			} else {
				return [];
			}
		} else {
			callback(null);
		}
	});
};




/**
 * Find possible not used username
 */
UserSchema.statics.findUniqueUsername = function(username, suffix, callback) {
	var _this = this;
	var possibleUsername = username + (suffix || '');

	_this.findOne({
		username: possibleUsername
	}, function(err, user) {
		if (!err) {
			if (!user) {
				callback(possibleUsername);
			} else {
				return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
			}
		} else {
			callback(null);
		}
	});
};

mongoose.model('User', UserSchema);
