'use strict';

/**
 * Module dependencies.
 */
exports.index = function(req, res) {
	res.render('index', {
		user: req.user || null
	});
};

exports.principal = function(req, res) {
if(req.user){
    res.render('principal', {
        user: req.user
    });
}else{
    res.render('index', {
        user: req.user || null
    });
}



};