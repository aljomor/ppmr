'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
    errorHandler = require('../errors'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    moment = require('moment'),
    jwt = require('jwt-simple'),
    config = require('../../../config/config'),
    User = mongoose.model('User');

/**
 * Update user details
 */
exports.me = function(req, res) {
    console.log(req.user);
    console.log(req.body);
    res.jsonp(req.user || null);
};

/**
 * Update user details
 */
exports.edit = function(req, res) {

    // Init Variables

    //var id = req.param('id');

    var token = req.headers.authorization;

    var userTokenData = null;

    if(token!==undefined){
        userTokenData = jwt.decode(token, config.sessionSecret);
    }else{
        res.status(400).send({
            message: 'No se ha podido comprobar su identidad.'
        });
    }

    var id = userTokenData.object;

    var message = null;

    // For security measurement we remove the roles from the req.body object
    delete req.body.user.roles;
    delete req.body.user.password;

    if (userTokenData && userTokenData.object!==undefined) {

        var reqUser = req.body.user;

        var modelUser = User.find({_id:id}, function(err, responseUsers){

            if(err){
                res.status(400).send({
                    error: err,
                    message: 'Este usuario no se encuentra registrado.'
                });
            }else{

                var responseUser = responseUsers[0];
                console.log('UserOrigin');
                console.log(responseUser);
                console.log('UserDataChange');
                console.log(reqUser);
                reqUser.password = responseUser.password;
                // Merge existing user
                responseUser = _.extend(responseUser, reqUser);
                console.log('UserMerged');
                console.log(responseUser);
                responseUser.updated = Date.now();
                responseUser.displayName = reqUser.firstName + ' ' + reqUser.lastName;

                User.update({_id: responseUser._id}, responseUser, {upsert: true}, function(err) {
                    if (err) {
                        return res.status(400).send({
                            error:  err,
                            message: errorHandler.getErrorMessage(err)
                        });
                    } else {
                        return res.status(200).json({
                            payload : responseUser,
                            message : 'Actualización exitosa.'
                        });
                    }
                });

            }
        });

    } else {
        res.status(400).send({
            message: 'El Usuario no se encuentra logueado.'
        });
    }
};
