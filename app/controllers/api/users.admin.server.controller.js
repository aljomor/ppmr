'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
    errorHandler = require('../errors'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    moment = require('moment'),
    jwt = require('jwt-simple'),
    config = require('../../../config/config'),
    User = mongoose.model('User');

/**
 * Update user details
 */
exports.list = function(req, res) {
    // Init Variables
    var user = req.user;
    var message = null;

    // For security measurement we remove the roles from the req.body object
    delete req.body.roles;

    User.find()
        .sort('-created')
        .exec(
            function(err, customers) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(customers);
                }
            }
        );
};


/**
 * Register a new user
 */
exports.create = function(req, res) {
    // For security measurement we remove the roles from the req.body object
    delete req.body.roles;

    console.log(req.body.user);

    // Init Variables
    var user = new User(req.body.user);
    var message = null;

    // Add missing user fields
    user.provider = 'local';
    user.displayName = user.firstName + ' ' + user.lastName;

    // Then save the user
    user.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            // Remove sensitive data before login
            user.password = undefined;
            user.salt = undefined;

            res.status(200).json({
                payload :  user,
                message : 'api.users.create success'
            });
        }
    });
};
