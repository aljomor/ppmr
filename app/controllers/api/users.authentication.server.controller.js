'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
    errorHandler = require('../errors'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    moment = require('moment'),
    jwt = require('jwt-simple'),
    config = require('../../../config/config'),
    User = mongoose.model('User');


/**
 * Login user
 */
exports.login = function(req, res, next) {

    // Init Variables
    var user = req.body;

    // check user
    if (!user) {
        //send bad request
        return res.status(500).json({payload : {}, message : 'Usuario indefinido'});
    }

    var username = (user.username !== undefined)? user.username : false;
    var password = (user.password !== undefined)? user.password : false;

    // Grab user fields.
    if (!username || !password) {
        //send bad request
        return res.status(400).json({payload : {}, message : 'Usuario o Clave Inválida'});
    }
    passport.authenticate('local', function(err, user, info) {
        if (err || !user) {
            res.status(400).send(info);
        } else {
            // Remove sensitive data before login
            user.password = undefined;
            user.salt = undefined;

            req.login(user, function(err) {
                if (err) {
                    res.status(400).send(err);
                } else {

                    var expires = moment().add(30,'days').valueOf();
                    var _token = jwt.encode({
                         object: user._id,
                         iss: user.username,
                         exp: expires
                    }, config.sessionSecret);

                    // console.log(config.sessionSecret);
                    // console.log('/');
                    // console.log(expires);
                    // console.log('/');
                    // console.log(_token);
                    res.json({
                        payload : {
                            user : user,
                            token : _token
                        },
                        message : 'Autenticación exitosa'
                    });
                }
            });
        }
    })(req, res, next);
};



/**
 * Logout user
 */
exports.logout = function(req, res) {
    // Init Variables
    var user = req.user;
    req.logout();
    res.json({
        payload: {},
        message: 'Se ha cerrado la sesión exitosamente.'
    });
};
