'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');

/**
 * Extend user's controller
 */
module.exports = _.extend(
    require('./api/users.authentication'),
    // require('./api/users.authorization'),
    // require('./api/users.password'),
    require('./api/users.profile'),
    require('./api/users.admin')
);