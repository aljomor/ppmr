'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	errorHandler = require('../errors'),
	mongoose = require('mongoose'),
	passport = require('passport'),
    Estudiante = mongoose.model('Estudiante'),
    User = mongoose.model('User');

/**
 * FIND REPRESENTADOS
 */
exports.representados = function(req, res) {
	// Init Variables
	var user = req.user;
    var id = user.id;
	var message = null;

	// For security measurement we remove the roles from the req.body object
	delete req.body.roles;

	if (user) {

        Estudiante.representados(function(id,err,repre){
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
            });
            }else{
            res.jsonp(repre);
                }

        });
	} else {
		res.status(400).send({
			message: 'Debe Loggearse'
		});
	}
};
