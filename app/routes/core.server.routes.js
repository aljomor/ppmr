'use strict';

module.exports = function(app) {
	// Root routing
	var core = require('../../app/controllers/core');
	app.route('/principal').get(core.principal);
    app.route('/').get(core.index);
};