'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var estudiantes = require('../../app/controllers/estudiantes');

	// Estudiantes Routes
	app.route('/estudiantes')
		.get(estudiantes.mys)
		.post(users.requiresLogin, estudiantes.create);

	app.route('/estudiantes/:estudianteId')
		.get(estudiantes.read)
		.put(users.requiresLogin, estudiantes.hasAuthorization, estudiantes.update)
		.delete(users.requiresLogin, estudiantes.hasAuthorization, estudiantes.delete);

	// Finish by binding the Estudiante middleware
	app.param('estudianteId', estudiantes.estudianteByID);
};