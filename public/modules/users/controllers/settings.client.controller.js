'use strict';

angular.module('users').controller('SettingsController', ['$scope', '$http', '$location', 'Users', 'Authentication','Estudiantes','Representados',
	function($scope, $http, $location, Users, Authentication, Estudiantes, Representados) {
		$scope.user = Authentication.user;
        $scope.avatarEstudianteDefault='social/php/assets/images/people/100/22.png';
        $scope.avatarDefault='social/php/assets/images/people/100/22.png';
        $scope.Representados = Representados;
        $scope.classDetalle='widget col-sm-6';
        $scope.classRepresentados='col-sm-12';

        // If user is not signed in then redirect back home
		if (!$scope.user) $location.path('/');

		// Check if there are additional accounts 
		$scope.hasConnectedAdditionalSocialAccounts = function(provider) {
			for (var i in $scope.user.additionalProvidersData) {
				return true;
			}

			return false;
		};

		// Check if provider is already in use with current user
		$scope.isConnectedSocialAccount = function(provider) {
			return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
		};

		// Remove a user social account
		$scope.removeUserSocialAccount = function(provider) {
			$scope.success = $scope.error = null;

			$http.delete('/users/accounts', {
				params: {
					provider: provider
				}
			}).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.user = Authentication.user = response;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		// Update a user profile
		$scope.updateUserProfile = function(isValid) {
			if (isValid){
				$scope.success = $scope.error = null;
				var user = new Users($scope.user);
	
				user.$update(function(response) {
					$scope.success = true;
					Authentication.user = response;
				}, function(response) {
					$scope.error = response.data.message;
				});
			} else {
				$scope.submitted = true;
			}
		};

		// Change user password
		$scope.changeUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post('/users/password', $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.passwordDetails = null;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

        $scope.postPublicacion = function() {
            $scope.Representados.estudiantes = Estudiantes.query();
        };

        $scope.findMys = function() {
            $scope.Representados.estudiantes = Estudiantes.query();
        };

        $scope.seleccionarEstudiante=function(seleccionado){
            $scope.x=seleccionado;

            $scope.classDetalle='widget col-sm-12';
            $scope.classRepresentados='col-sm-6';
            $scope.Representados.estudianteActual = seleccionado;
            console.log( $scope.Representados.estudianteActual);
            if(  $scope.Representados.estudianteActual.cedula){
                 $scope.Representados.estudianteActual.cedula2 = 'CI:'+  $scope.Representados.estudianteActual.nacionalidad +  $scope.Representados.estudianteActual.cedula;
            }
            if(  $scope.Representados.estudianteActual.cedula_escolar ){
                 $scope.Representados.estudianteActual.cedula_escolar2 = 'CE:'+ $scope.Representados.estudianteActual.cedula_escolar;
            }
            if( $scope.Representados.estudianteActual.sexo==='F'){
                 $scope.Representados.estudianteActual.sexo = 'Femenino';
                 $scope.Representados.estudianteActual.sexoIcon = 'fa fa-female fa-fw text-muted text-primary';
            }
            if( $scope.Representados.estudianteActual.sexo==='M'){
                 $scope.Representados.estudianteActual.sexo = 'Masculino';
                 $scope.Representados.estudianteActual.sexoIcon = 'fa fa-male fa-fw text-muted text-primary';
            }
             $scope.Representados.estudianteActual.displayName =  $scope.Representados.estudianteActual.nombres +' '+ $scope.Representados.estudianteActual.apellidos;
            jQuery('#detalleRepresentado').fadeOut('fast').ready(function(){
            jQuery('#detalleRepresentado').fadeIn('fast');
            });
        };
        $scope.items = [
            'The first choice!',
            'And another choice for you.',
            'but wait! A third!'
        ];

    }
]).
    filter('capitalize', function() {
        return function(input, all) {
            return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
        };
    }).filter('unsafe', ['$sce', function ($sce) {
        return function (val) {
            return $sce.trustAsHtml(val);
        };
    }]);

angular.module('template/popover/popover.html', []).run(['$templateCache', function ($templateCache) {
    $templateCache.put('template/popover/popover.html',
            '<div class=\"popover {{placement}}\" ng-class=\"{ in: isOpen(), fade: animation() }\">\n' +
            '  <div class=\"arrow\"></div>\n' +
            '\n' +
            '  <div class=\"popover-inner\">\n' +
            '      <h3 class=\"popover-title\" ng-bind-html=\"title | unsafe\" ng-show=\"title\"></h3>\n' +
            '      <div class=\"popover-content\"ng-bind-html=\"content | unsafe\"></div>\n' +
            '  </div>\n' +
            '</div>\n' +
            '');
}]);
