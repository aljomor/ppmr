'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', 'Authentication',
	function($scope, $http, $location, Authentication) {
        $scope.mascara= 'V-99999999';/* mascara */
      $scope.vzlno = function(){
          $scope.mascara = 'V-99999999';

      };
        $scope.pasaporte = function(){
            $scope.mascara = '99999999';

        };
      $scope.extranjero = function(){
          $scope.mascara = 'E-99999999';

      };
		$scope.authentication = Authentication;

		// If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/principal');

		$scope.signup = function() {
			$http.post('/auth/signup', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$location.path('/principal');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		$scope.signin = function() {
			$http.post('/auth/signin', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$location.path('/principal');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);