'use strict';

angular.module('users').controller('YoController', ['$scope', '$stateParams', '$location', 'Authentication', 'Articles','Estudiantes',
    function($scope, $stateParams, $location, Authentication, Articles, Estudiantes) {
        $scope.authentication = Authentication;

        $scope.find = function() {
            $scope.articles = Articles.query();
        };

        $scope.findOne = function() {
            $scope.article = Articles.get({
                articleId: $stateParams.articleId
            });
        };


        $scope.findMys = function() {
//            console.log( $scope.authentication.user.id);
            $scope.representados = Estudiantes.get();
        };

    }
]);